package com.example.flabe.pocapp;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

/**
 * Created by flabe on 6/5/2018.
 */

public class connection extends Activity {

    static final int STATE_LISTENING = 1;
    static final int STATE_CONNECTING = 2;
    static final int STATE_CONNECTED = 3;
    static final int STATE_CONNECTION_FAILED = 4;
    static final int STATE_MESSAGE_RECEIVED = 5;

    String type = "";

    ArrayList<String> devices = new ArrayList<String>();
    ArrayAdapter<String> adapter;


    int actual_index = 0;
    int msg_send = 0;

    BluetoothAdapter myBluetoothAdapter;
    BluetoothDevice[] btArray;

    ListView listView;
    TextView textView;
    TextView msg_box;
    Button listen;
    Button send;

    SendReceive sendReceive;
    ServerClass serverClass;

    private static final String APP_NAME = "BluetoothComm";
    ArrayList<UUID> mUuids;

    int teste  = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connections);

        Intent intent = getIntent();

        type = intent.getStringExtra(MainActivity.SERVER_TYPE);

        myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        findViewByIds();

        textView.setText(type);

        mUuids = new ArrayList<UUID>();
        mUuids.add(UUID.fromString("658fcda0-3433-11e8-b467-0ed5f89f718b"));
        mUuids.add(UUID.fromString("b7746a40-c758-4868-aa19-7ac6b3475dfc"));
        mUuids.add(UUID.fromString("2d64189d-5a2c-4511-a074-77f199fd0834"));
        mUuids.add(UUID.fromString("e442e09a-51f3-4a7b-91cb-f638491d1412"));
        mUuids.add(UUID.fromString("a81d6504-4536-49ee-a475-7d96d09439e4"));

        implementListeners();
    }

    public void findViewByIds() {
        listView = (ListView) findViewById(R.id.listView);
        textView = findViewById(R.id.textView);
        listen = (Button)findViewById(R.id.listen);
        send = (Button)findViewById(R.id.send);
        msg_box = (TextView)findViewById(R.id.msg_box);
    }

    public void implementListeners() {
        if (type.equals("Slave")) {
            textView.setText("ENTREI");
            Set<BluetoothDevice> bt = myBluetoothAdapter.getBondedDevices();
            btArray = new BluetoothDevice[bt.size()];
            String[] strings = new String[bt.size()];
            int index = 0;

            if (bt.size() > 0) {
                for (BluetoothDevice device : bt) {
                    btArray[index] = device;
                    strings[index] = device.getName();
                    index++;
                }
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, strings);
                listView.setAdapter(arrayAdapter);
            }
            listen.setVisibility(View.GONE);
        }

        if (type.equals("Master")){
            serverClass=new ServerClass();
            textView.setText("Listening Connections");

            adapter=new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1,
                    devices);

            listView.setAdapter(adapter);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try{
                    textView.setText("Connecting");
                    for(int i = 0; i < 4; i++) {
                        ClientClass clientClass = new ClientClass(btArray[position], mUuids.get(i));
                        clientClass.start();
                    }
                }
                catch (Exception e){
                    textView.setText("Failed");
                }
            }
        });

        listen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serverClass.listenConn();
                actual_index++;
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string= "teste" + Integer.toString(msg_send);
                msg_send++;
                if (type == "Master") {

                } else {
                    sendReceive.write(string.getBytes());
                }
            }
        });
    }

    Handler handler= new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg){
            switch (msg.what){
                case STATE_LISTENING:
                    textView.setText("listening");
                    break;
                case STATE_CONNECTING:
                    textView.setText("connecting");
                    break;
                case STATE_CONNECTED:
                    textView.setText("connected");
                    break;
                case STATE_CONNECTION_FAILED:
                    textView.setText("failed");
                    break;
                case STATE_MESSAGE_RECEIVED:
                    byte[] readBuffer= (byte[])msg.obj;
                    String tempMsg=new String(readBuffer, 0,msg.arg1);
                    Long tsLong = System.currentTimeMillis();
                    String ts = tsLong.toString();
                    msg_box.setText(tempMsg);
                    textView.setText("message received");
                    break;
                case 10:
                    textView.setText(Integer.toString(teste));
                    teste++;
                    break;
                case 11:
                    break;
            }
            return true;
        }
    });

    private class ServerClass extends Thread{
        private BluetoothServerSocket serverSocket = null;

        public ServerClass(){

        }

        public void listenConn(){
            BluetoothSocket socket = null;
            int index = 0;
            try {
                serverSocket = myBluetoothAdapter.listenUsingRfcommWithServiceRecord(APP_NAME, mUuids.get(actual_index));
                socket = serverSocket.accept();

                if (socket != null) {
                    String address = socket.getRemoteDevice().getAddress();

                    devices.add(address);
                    adapter.notifyDataSetChanged();

                    Message message = Message.obtain();
                    message.what=STATE_CONNECTED;
                    handler.sendMessage(message);

                    sendReceive = new SendReceive(socket);
                    sendReceive.start();

                    listen.setVisibility(View.VISIBLE);
                }

            } catch (IOException e) {
                Message message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                handler.sendMessage(message);
                e.printStackTrace();
            }
        }
    }

    private class ClientClass extends Thread{
        private BluetoothDevice device;
        private BluetoothSocket socket;

        public ClientClass(BluetoothDevice device1, UUID uuidToTry){
            device = device1;
            try {
                socket=device.createRfcommSocketToServiceRecord(uuidToTry);
            } catch (IOException e) {
                Message message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                handler.sendMessage(message);
                e.printStackTrace();
            }

        }
        public void run(){
            try {
                socket.connect();
                Message message = Message.obtain();
                message.what = STATE_CONNECTED;
                handler.sendMessage(message);

                sendReceive = new SendReceive(socket);
                sendReceive.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class SendReceive extends Thread{
        private final BluetoothSocket bluetoothSocket;
        private final InputStream inputStream;
        private final OutputStream outputStream;
        ArrayList<BluetoothSocket> btSockets = new ArrayList<BluetoothSocket>();

        public SendReceive(BluetoothSocket socket){
            bluetoothSocket = socket;
            btSockets.add(bluetoothSocket);

            InputStream tempIn = null;
            OutputStream tempOut = null;

            try {
                tempIn = bluetoothSocket.getInputStream();
                tempOut = bluetoothSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            inputStream=tempIn;
            outputStream=tempOut;
        }

        public void run(){
            byte[] buffer=new byte[1024];
            int bytes;

            while(true){
                try {
                    bytes = inputStream.read(buffer);
                    handler.obtainMessage(STATE_MESSAGE_RECEIVED, bytes, -1, buffer).sendToTarget();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void write(byte[] bytes){
            try {
                System.out.println("bytes" + bytes);
                outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
